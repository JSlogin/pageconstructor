import mongoose from 'mongoose';

const Schema =  mongoose.Schema;

const DocumentSchema = new Schema({
    title: { type: String, required: true },
    resolution: { type: String },
    updated: { type: Date, default: Date.now },
    canvas: {type: [] }
})

const Document = mongoose.model('Document', DocumentSchema);