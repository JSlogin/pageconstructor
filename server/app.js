import express from 'express';
import bodyParser from 'body-parser';

import * as db from './utils/DataBaseUtils';

import { serverPort } from '../etc/config.json';

db.setUpConnection();

const app = express();

app.use( bodyParser.json() );

app.get( '/documents', (req, res) => {
    db.listDocuments()
    .then( data => res.send(data) );
});

app.post( '/documents', (req, res) => {
    db.createDocument(req.body)
    .then( data => res.send(data) );
});

app.delete( 'documents/:id', (req, res) => {
    db.deleteDocument(req.params.id)
    .then(data => res.send(data));
});

const server = app.listen(serverPort, () => {
    console.log(`server in runing on port ${serverPort}`);
})