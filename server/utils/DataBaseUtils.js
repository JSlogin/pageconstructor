import mongoose from 'mongoose';

import '../models/Document';

import config from '../../etc/config.json';

const Documents = mongoose.model('Document');

export function setUpConnection() {
    mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.name}`);
};

export function listDocuments() {
    return Documents.find();
};

export function createDocument(data) {
    const document = new Documents({
        title: data.title,
        resolution: data.resolution,
        updated: new Date(),
        canvas: data.canvas
    });

    return document.save();
};

export function deleteDocument(id) {
    return Document.findById(id).remove();
}