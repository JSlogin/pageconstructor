import React from 'react';

import DocumentItem from './DocumentItem';

class DocumentList extends React.Component{
    
    
    render(){
        console.log(this.props.documents)
        return(
            <div>
                <h2>Document List</h2>
                <DocumentItem />
            </div>
        );
    };
};

export default DocumentList;