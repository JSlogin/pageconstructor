import React from 'react';

import DocumentList from './DocumentList';
import DocumentEditor from './DocumentEditor';

class App extends React.Component{

    constructor(props){
        super(props);

        this.state = { data: {}}
        this.handleDocumentAdd = this.handleDocumentAdd.bind(this)
    }

    handleDocumentAdd(data) {
        this.setState({data});
    }
    
    render(){
        return(
            <div>
                <h1>APP</h1>
                <DocumentEditor onDocumentAdd={this.handleDocumentAdd}/>
                <DocumentList documents={this.state.data} />
            </div>
        );
    };
};

export default App;