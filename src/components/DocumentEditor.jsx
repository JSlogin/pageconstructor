import React from 'react';

class DocumentEditor extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            title: '',
            resolution: '1240x960',
            updated: new Date(),
            canvas: []
        }
        
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleResolutionChange = this.handleResolutionChange.bind(this);
        this.handleDocumentAdd = this.handleDocumentAdd.bind(this);
    };

    handleTitleChange(event) {
        this.setState({title: event.target.value});
    };

    handleResolutionChange(event) {
        this.setState({resolution: event.target.value});
    };

    handleDocumentAdd() {
        const newDocument = {
            title: this.state.title,
            resolution: this.state.resolution,
            updated: this.state.updated,
            canvas: []
        };

        this.props.onDocumentAdd(newDocument);
        this.setState({ title: '', resolution: '', updated: new Date(), canvas: [] });
    };

    render(){
        return(
            <div>
                <h3>DocumentEditor</h3>
                <input 
                    type="text" 
                    value={this.state.title} 
                    onChange={this.handleTitleChange}
                />
                
                <select value={this.state.value} onChange={this.handleResolutionChange}>
                    <option value="1240x960">1240x960</option>
                    <option value="740x500">740x500</option>
                </select>
                <button
                    disabled={!this.state.title}
                    onClick={this.handleDocumentAdd}
                >
                    Add
                </button>
            </div>
        );
    };
};

export default DocumentEditor;